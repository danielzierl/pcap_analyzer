FROM golang:1.21-bookworm
# Set the working directory inside the container
WORKDIR /app
ENV CGO_ENABLED=1
# Copy the Go module files to the working directory
COPY . .
# Download and install the Go dependencies
RUN go mod download 
# Build the Go application
RUN apt update
RUN apt install libpcap-dev -y
RUN go build -o pcap_analyzer ./cmd/main/main.go
# Expose a port if your Go application listens on a specific port
# Set the command to run your Go application
CMD ["./pcap_analyzer"]
