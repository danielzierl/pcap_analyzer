package main

import (
	"fmt"

	"gitlab.com/danielzierl/pcap_analyzer/internal"
)


func main(){
  var input string

  printInfo()
  printSeparator()
  fmt.Print("Enter pcap location: ")
  fmt.Scanln(&input)
  packets:=internal.LoadPcapFile(input)
  packet_count:=internal.GetPacketsCount(packets)
  printSeparator()
  fmt.Printf("n_packets: %d \n",packet_count)
  printSeparator()
  fmt.Printf("ip: %s \n",internal.GetMostProbableIpAddress(packets))
  printSeparator()
  fmt.Println("visited:")
  internal.PrintVisitedRecords(packets)
}
func printSeparator(){
  fmt.Println("-------------------------")
}
func printInfo(){
  fmt.Print("\nPcap file analyzer\nAuthor: Daniel Zierl\n")
}
