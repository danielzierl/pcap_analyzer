package internal

import (
	"log"

	"github.com/google/gopacket"
	"github.com/google/gopacket/pcap"
)

func LoadPcapFile(filename string)[]gopacket.Packet{
  handle, err := pcap.OpenOffline(filename)
  if err != nil {
    log.Fatal(err)
  }
  defer handle.Close()
  packetSource := gopacket.NewPacketSource(handle, handle.LinkType())
  var packets []gopacket.Packet
  for packet := range packetSource.Packets() {
    packets = append(packets, packet)
  }
  return packets

}
