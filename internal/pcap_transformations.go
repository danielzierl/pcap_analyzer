package internal

import (
	"fmt"
	"strings"
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
)


func GetPacketsCount(packets []gopacket.Packet) int{
  return len(packets)
}
func GetMostProbableIpAddress(packets []gopacket.Packet)string{
  var ipMap map[string]int = make(map[string]int)
  for _,packet := range packets {
    network_layer_packet := packet.NetworkLayer()
		if network_layer_packet== nil {
			continue
		}
    src :=network_layer_packet.NetworkFlow().Src().String()
    dest :=network_layer_packet.NetworkFlow().Dst().String()
    ipMap[src]+=1
    ipMap[dest]+=1
  }
  return getBiggestKeytoNumberInMap(ipMap)
}
func getBiggestKeytoNumberInMap(m map[string]int)string{
  max_n:=0
  var key_n string
  for key,val:= range m{
    //lets skip multicast
    split:=strings.Split(key, "::")
    if split[len(split)-1]=="fb" {
      continue
    }
    if val>max_n{
      
      max_n=val
      key_n=key
    }
  }
  return  key_n
}
func getVisitedRecord(packet gopacket.Packet,dns layers.DNS)string{
    var question = dns.Questions[0]
    return fmt.Sprintf("%s %s %s\n",packet.NetworkLayer().NetworkFlow().Src().String(), packet.NetworkLayer().NetworkFlow().Dst().String(), question)
}
func PrintVisitedRecords(packets []gopacket.Packet){
  for  _,  packet := range packets {
    dnsLayer := packet.Layer(layers.LayerTypeDNS);
    if dns, ok := dnsLayer.(*layers.DNS); ok {
      fmt.Print(getVisitedRecord(packet, *dns))
    }
  }
}

