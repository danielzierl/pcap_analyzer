import subprocess
import platform


def main(image_name):
    choice = get_user_input()
    run_docker_image(image_name, choice)


def get_user_input():
    print(f"Select how to run: \n1) build and run docker file (recommended)\n2) only run (select if docker image is already build)")
    while 1:
        try:
            user_resp = input()
            user_resp_int: int = int(user_resp)
            break
        except:
            print("please select 1 or 2")
    return user_resp_int


def run_docker_image(image_name, choice: int):
    # Build the Docker image
    try:
        if (choice == 1):
            subprocess.run(['docker', 'build', '-t', image_name, '.'])
    except Exception as e:
        print('Error building dockerfile, do you have docker installed? Error:', e)
    try:
        ...  # clear_console()
    except Exception as e:
        print('clearing console failed')
    # Run the Docker container

    subprocess.run(['docker', 'run', '-it', '--rm', image_name])


def clear_console():
    # Get the current operating system
    os = platform.system()
    # Clear console based on the operating system
    if os == 'Windows':
        subprocess.run('cls', shell=True)
    else:
        subprocess.run('clear', shell=True)


if __name__ == '__main__':
    image_name = 'pcap_analyzer'
    main(image_name)
